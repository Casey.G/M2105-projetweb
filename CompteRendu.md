Compte rendu TP écrit par Casey GRONDIN


IDENTIFICATION DU BESOIN :

– Fiche de présence qui va permettre de gérer la présence des étudiants aux cours.
–  Utilisation limité au département RT pour cette phase (Utilisable par tous les départements de
    l’IUT) 
– Trois catégories d’utilisateurs : – Étudiants
				                    – Enseignants
			 	                    – Personnel administratif

ANALYSE FONCTIONNELLE :

L’outil permettra de saisir la liste des élèves présents ou absents à chaque séance de chaque module.
C’est le professeur responsable du module qui ouvrira la saisie pour une séance donnée.
La saisir pour être fait par le professeur ou par les élèves.

ABSENCE :
L’outil permettra de visualiser le taux d’absentéisme par élève pour chaque semestre. 
Les élèves présentant un taux supérieur à 10 % seront identifiés à l’affichage.
Affichage sous forme de tableau ou de graphe.

Seul le personnel administratif peut visualiser l’ensemble des données pour tous étudiants et tous les modules.
Le personnel administratif sera aussi en charge de l’administration de l’outil.


ARBORESCENCE DU SITE :
– Une page d’accueil pour pouvoir se connecter
– Une page pour chaque utilisateurs : Étudiants/Professeurs/Personnel administratif

CONTENU DES PAGES :
Étudiants :
Dans un coin de la page il y aura le pourcentage d’absentéisme pour le semestre en cour
Au centre de la page il y aura la liste des étudiants lorsque le prof activera la liste pour pouvoir cocher présent ( à revoir plus tard )


PROFESSEURS :
Une page contenant la liste des étudiants concerné par le cours en fonction des TD/TP/Cours
Sur cette page il pourra noter les absences/retards


PERSONNEL ADMINISTRATIF :
– Une page d’accueil qui contiendra un menu avec :
	– Un onglet pour les TD
	– Un onglet pour les TP
	– Un onglet pour les cours
	– Un onglet par module
– On pourra consulter le pourcentage d’absence de chaque élève par le biais d’un autre onglet
– Onglet de configuration qui permettra de rajouter ou enlever des modules/enseignants/élèves…



Conception du site
******************************************************
*                   Base de donnée                   *
******************************************************

Après avoir fait l'analyse des besoins, jai donc commencé la création de la base de donnée.
Au début j'ai commencer avec une table pour chaque type d'utilisateur et une table pour noter les absences : total de 4tables (Admin, prof, étudiant et presence).

Après réflexion et vu que deux professeurs ont dis que l'on pouvait le faire avec seulement deux tables, j'ai décidé de recommencer ma base de donnée pour simplifier
donc une table user et une table presence.

Cependant après avoir commencé d'écrire le code, lors de l'utilisation d'une requête je me suis rendu compte qu'il y avait une erreur car quand le professeur 
devait saisir le td ou tp il fallait que la requête sur la base de donnée vérifie les champs de la BDD dans la colonne TD et TP. Mais pour les professeurs et les administrateurs
le champ td et tp était vide.

C'est pourquoi au final je suis revenu sur ma base de donnée de départ pour que tout fonctionne.



***************************************************
*                   Page php                      *
***************************************************

J'ai tout d'abord identifié le besoin de gérer les connexions qu'ils peuvent y avoir en fonction des différents utilisateurs.

Ensuite j'ai créé les pages qui permettent à chaque utilisateurs de partir sur leurs pages respectivent.

Sur la page professeurs il a fallut créer un menu pour sélectionner les différents groupes et modules pour pouvoir afficher la bonne liste d'élèves.

Sur la page des étudiants j'ai fait un code qui permet de calculer le pourcentage d'absence de chaque élève en fonction des données déjà recueilli dans la table presence de la base
de la base de donnée.

La page admin je n'ai pas pu la terminer par manque de temps.

Pour finir vu qu'on se connecte il faut une fonction pour pouvoir se déconnecter, je l'ai donc faîte.


**************************************************
*                       CSS                      *
**************************************************

J'ai fais un CSS pour la page de connexion ainsi que pour les pages élèves et professeurs.