-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Ven 16 Juin 2017 à 13:34
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `pronote`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE `admin` (
  `NumAdmin` varchar(12) NOT NULL,
  `NomAdmin` varchar(15) NOT NULL,
  `PrenomAdmin` varchar(15) NOT NULL,
  `MailAdmin` varchar(30) NOT NULL,
  `PassAdmin` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Repertoire des Admins ';

--
-- Contenu de la table `admin`
--

INSERT INTO `admin` (`NumAdmin`, `NomAdmin`, `PrenomAdmin`, `MailAdmin`, `PassAdmin`) VALUES
('adm1', 'MOREL', 'Johanne', 'j.morel@rt-iut.re', 'morel');

-- --------------------------------------------------------

--
-- Structure de la table `enseignant`
--

CREATE TABLE `enseignant` (
  `NumProf` varchar(12) NOT NULL,
  `NomProf` varchar(15) NOT NULL,
  `PrenomProf` varchar(15) NOT NULL,
  `MailProf` varchar(30) NOT NULL,
  `PassProf` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Repertoire des enseignants';

--
-- Contenu de la table `enseignant`
--

INSERT INTO `enseignant` (`NumProf`, `NomProf`, `PrenomProf`, `MailProf`, `PassProf`) VALUES
('pro1', 'TURQUAY', 'David', 'd.turquay@rt-iut.re', 'turquay'),
('pro2', 'RAZAFINDRALAMBO', 'Tahiry', 't.raza@rt-iut.re', 'tahiry'),
('pro3', 'GROUFFAUD', 'Joel', 'j.grouffaud@rt-iut.re', 'grouffaud'),
('pro4', 'JONES', 'Dave', 'd.jones@rt-iut.re', 'jones'),
('pro5', 'CHANE', 'Laurent', 'l.chane@rt-iut.re', 'chane'),
('pro6', 'COMBE', 'Patrick', 'p.combe@rt-iut.re', 'combe');

-- --------------------------------------------------------

--
-- Structure de la table `etudiant`
--

CREATE TABLE `etudiant` (
  `NumEtudiant` varchar(12) NOT NULL,
  `NomEtudiant` varchar(15) NOT NULL,
  `PrenomEtudiant` varchar(15) NOT NULL,
  `MailEtudiant` varchar(30) NOT NULL,
  `PassEtudiant` varchar(16) NOT NULL,
  `Annee` int(1) NOT NULL,
  `TD` int(1) NOT NULL,
  `TP` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `etudiant`
--

INSERT INTO `etudiant` (`NumEtudiant`, `NomEtudiant`, `PrenomEtudiant`, `MailEtudiant`, `PassEtudiant`, `Annee`, `TD`, `TP`) VALUES
('350001', 'CERVEAUX', 'Dylan', 'd.cerveaux@rt-iut.re', 'cerveaux', 2, 1, 1),
('350002', 'CHAMAND', 'Etienne', 'e.chamand@rt-iut.re', 'chamand', 2, 1, 1),
('350003', 'CLAIN', 'Louis', 'l.clain@rt-iut.re', 'clain', 2, 1, 1),
('350004', 'DORMEUIL', 'Benjamin', 'b.dormeuil@rt-iut.re', 'dormeuil', 2, 1, 2),
('350005', 'DRAGON', 'Gulian', 'g.dragon@rt-iut.re', 'dragon', 2, 1, 2),
('350006', 'FLORICOURT', 'Quentin', 'q.floricourt@rt-iut.re', 'floricourt', 2, 2, 2),
('350007', 'FOLLET', 'Melvin', 'm.follet@rt-iut.re', 'follet', 2, 2, 3),
('350008', 'HOAREAU', 'Philippe', 'p.hoareau@rt-iut.re', 'hoareau', 2, 2, 3),
('360001', 'ADENOR', 'Bryan', 'b.adenor@rt-iut.re', 'adenor', 1, 1, 1),
('360002', 'ALIDOR', 'Ludovic', 'l.alidor@rt-iut.re', 'alidor', 1, 1, 1),
('360003', 'AUDIGIER', 'Mael', 'm.audigier@rt-iut.re', 'audigier', 1, 1, 1),
('360004', 'BABET', 'Florent', 'f.babet@rt-iut.re', 'babet', 1, 1, 2),
('360005', 'BACO', 'Sabrina', 's.baco@rt-iut.re', 'baco', 1, 2, 2),
('360006', 'BERNARD', 'Fabien', 'f.bernard@rt-iut.re', 'bernard', 1, 2, 2),
('360007', 'CARO', 'Bastien', 'b.caro@rt-iut.re', 'caro', 1, 2, 3),
('360010', 'GEREONE', 'Leo', 'l.gereone@rt-iut.re', 'gereone', 1, 2, 3),
('360011', 'GRONDIN', 'Casey', 'c.grondin@rt-iut.re', 'grondin', 1, 2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `presence`
--

CREATE TABLE `presence` (
  `Id` varchar(20) NOT NULL,
  `Nom` varchar(15) NOT NULL,
  `Prenom` varchar(15) NOT NULL,
  `Absent` int(2) NOT NULL,
  `Le` date NOT NULL,
  `Heure` varchar(5) NOT NULL,
  `NumEtu` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `presence`
--

INSERT INTO `presence` (`Id`, `Nom`, `Prenom`, `Absent`, `Le`, `Heure`, `NumEtu`) VALUES
('16-06-2017-04-27-161', 'BABET', 'Florent', 0, '2017-06-16', '16-27', 360004),
('16-06-2017-04-27-162', 'BACO', 'Sabrina', 1, '2017-06-16', '16-27', 360005),
('16-06-2017-04-27-163', 'BERNARD', 'Fabien', 0, '2017-06-16', '16-27', 360006),
('16-06-2017-04-28-471', 'BABET', 'Florent', 0, '2017-06-16', '16-28', 360004),
('16-06-2017-04-28-473', 'BERNARD', 'Fabien', 0, '2017-06-16', '16-28', 360006),
('16-06-2017-04-30-311', 'BABET', 'Florent', 0, '2017-06-16', '16-30', 360004),
('16-06-2017-04-30-312', 'BACO', 'Sabrina', 0, '2017-06-16', '16-30', 360005),
('16-06-2017-04-30-323', 'BERNARD', 'Fabien', 0, '2017-06-16', '16-30', 360006),
('16-06-2017-04-31-451', 'BABET', 'Florent', 0, '2017-06-16', '16-31', 360004),
('16-06-2017-04-31-462', 'BACO', 'Sabrina', 1, '2017-06-16', '16-31', 360005),
('16-06-2017-04-31-463', 'BERNARD', 'Fabien', 0, '2017-06-16', '16-31', 360006),
('16-06-2017-04-43-401', 'BABET', 'Florent', 0, '2017-06-16', '16-43', 360004),
('16-06-2017-04-43-412', 'BACO', 'Sabrina', 0, '2017-06-16', '16-43', 360005),
('16-06-2017-04-43-413', 'BERNARD', 'Fabien', 0, '2017-06-16', '16-43', 360006);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`NumAdmin`),
  ADD UNIQUE KEY `NomAdmin` (`NomAdmin`),
  ADD UNIQUE KEY `PrenomAdmin` (`PrenomAdmin`),
  ADD UNIQUE KEY `PassAdmin` (`PassAdmin`),
  ADD KEY `NomAdmin_2` (`NomAdmin`,`PrenomAdmin`,`MailAdmin`,`PassAdmin`);

--
-- Index pour la table `enseignant`
--
ALTER TABLE `enseignant`
  ADD PRIMARY KEY (`NumProf`),
  ADD UNIQUE KEY `NomProf` (`NomProf`),
  ADD UNIQUE KEY `MailProf` (`MailProf`),
  ADD UNIQUE KEY `PrenomProf` (`PrenomProf`),
  ADD UNIQUE KEY `PassProf` (`PassProf`),
  ADD KEY `NomProf_2` (`NomProf`),
  ADD KEY `PrenomProf_2` (`PrenomProf`),
  ADD KEY `MailProf_2` (`MailProf`),
  ADD KEY `PassProf_2` (`PassProf`);

--
-- Index pour la table `etudiant`
--
ALTER TABLE `etudiant`
  ADD PRIMARY KEY (`NumEtudiant`),
  ADD UNIQUE KEY `NomEtudiant` (`NomEtudiant`),
  ADD UNIQUE KEY `PrenomEtudiant` (`PrenomEtudiant`),
  ADD UNIQUE KEY `MailEtudiant` (`MailEtudiant`),
  ADD UNIQUE KEY `PassEtudiant` (`PassEtudiant`),
  ADD KEY `NomEtudiant_2` (`NomEtudiant`,`PrenomEtudiant`,`MailEtudiant`,`PassEtudiant`),
  ADD KEY `TD` (`TD`),
  ADD KEY `TP` (`TP`),
  ADD KEY `Année` (`Annee`);

--
-- Index pour la table `presence`
--
ALTER TABLE `presence`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Nom` (`Nom`),
  ADD KEY `Prenom` (`Prenom`),
  ADD KEY `Absent` (`Absent`),
  ADD KEY `#NumEtudiant` (`NumEtu`),
  ADD KEY `Le` (`Le`),
  ADD KEY `Heure` (`Heure`),
  ADD KEY `Id` (`Id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
