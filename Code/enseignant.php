<?php

	session_start();
	if(($_SESSION['prof'] != 1)){
		sleep(1);
		header('Location: login.php?login='.$_POST['identifiant']); //Redirection si non authentifié
	}

	// Connexion à la BDD rt_check
	try {
		$bdd = new PDO('mysql:host=localhost;dbname=pronote', 'root', '');
		
	}
	catch (exception $e){
		echo ("Erreur de connexion! Base de données inaccessible <br/>".$e) ;
	}
?>

<!DOCTYPE html>
	<head>
		<link href="prof.css" rel="stylesheet">
		<title> Enseignant </title>
		<meta charset = "utf-8">
	</head>

	<header>
		Cahier d'appel électronique
		<div class="logout">
			<form method='post' action="logout.php">
				<input type="submit" value="Se déconnecter" name="logout"></input>
			</form>
		</div>
	</header>

	<hr>

	<body>
		<?php
			date_default_timezone_set('Indian/Reunion'); //Définir fuseau horaire
			echo ("Nous sommes le :".date('d-m-Y')."<br>"); // Obtenir la date
			echo ("Il est : ".date('H')."h ".date('i')); //Obtenir l'heure
		?>

		<h1> Page ENSEIGNANT </h1>

		<div class="menu">
			<form method='post' action="presence.php">
				Module : <select name="module">
							<option value="M1101">M1101</option>
							<option value="M1102">M1102</option>
							<option value="M1103">M1103</option>
							<option value="M1104">M1104</option>
							<option value="M1105">M1105</option>
							<option value="M2101">M2101</option>
							<option value="M2102">M2102</option>
							<option value="M2103">M2103</option>
							<option value="M2104">M2104</option>
							<option value="M2105">M2105</option>
						</select>	

				Heure de début: 
						<select name="heuredeb">
							<option value="8:00">8:00</option>
							<option value="8:30">8:30</option>
							<option value="9:00">9:00</option>
							<option value="9:30">9:30</option>
							<option value="10:00">10:00</option>
							<option value="10:30">10:30</option>
							<option value="11:00">11:00</option>
							<option value="11:30">11:30</option>
							<option value="12:00">12:00</option>
							<option value="12:30">12:30</option>
							<option value="13:00">13:00</option>
							<option value="13:30">13:30</option>
							<option value="14:00">14:00</option>
							<option value="14:30">14:30</option>
							<option value="15:00">15:00</option>
							<option value="15:30">15:30</option>
							<option value="16:00">16:00</option>
							<option value="16:30">16:30</option>
							<option value="17:00">17:00</option>
							<option value="17:30">17:30</option>
							<option value="18:00">18:00</option>
							<option value="18:30">18:30</option>
						</select>

				Heure de fin : 
						<select name="heurefin">
							<option value="8:30">8:30</option>
							<option value="9:00">9:00</option>
							<option value="9:30">9:30</option>
							<option value="10:00">10:00</option>
							<option value="10:30">10:30</option>
							<option value="11:00">11:00</option>
							<option value="11:30">11:30</option>
							<option value="12:00">12:00</option>
							<option value="12:30">12:30</option>
							<option value="13:00">13:00</option>
							<option value="13:30">13:30</option>
							<option value="14:00">14:00</option>
							<option value="14:30">14:30</option>
							<option value="15:00">15:00</option>
							<option value="15:30">15:30</option>
							<option value="16:00">16:00</option>
							<option value="16:30">16:30</option>
							<option value="17:00">17:00</option>
							<option value="17:30">17:30</option>
							<option value="18:00">18:00</option>
							<option value="18:30">18:30</option>
							<option value="19:00">19:00</option>
						</select>
				Année : <select name="annee">
							<option value="1">RT1</option>
							<option value="2">RT2</option>
							<option value="3">LPro</option>
						</select>

				Groupe : <select name="groupe">
							<option value="0">CM</option>
							<option value="11">TD1</option>
							<option value="12">TD2</option>
							<option value="21">TP1</option>
							<option value="22">TP2</option>
							<option value="23">TP3</option>
						</select>
				<input type="submit" name="creer" value="Créer">
			</form>
		</div>

	</body>
</html>