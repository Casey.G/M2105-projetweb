<?php
	session_start();
	try {
		$bdd = new PDO('mysql:host=localhost;dbname=pronote', 'root', '');
		
	}
	catch (exception $e){
		echo ("Erreur de connexion! Base de données inaccessible <br/>".$e) ;
	}

	// Informations du formulaire
	$module = $_POST['module'];
	$annee = $_POST['annee'];

	if ($_POST['groupe'] == 0){
			$etu = $bdd -> query("SELECT * From presence, etudiant WHERE NumEtu = NumEtudiant AND Annee = '$annee'");
			$groupe = "CM";
	}

	if ($_POST['groupe'] > 9 && $_POST['groupe'] < 19 ){
		// c'est un groupe de TD et le numero du groupe est : gr = $_POST['groupe'] - 10
			$gr = $_POST['groupe'] - 10;
			$groupe = 'TD'.$gr;
			$etu = $bdd -> query("SELECT * From presence, etudiant WHERE NumEtu = NumEtudiant AND Annee = '$annee' AND TD = '$gr'");
	}

	if ($_POST['groupe'] > 19 && $_POST['groupe'] < 29 ){
		// c'est un groupe de TP et le numero du groupe est : gr = $_POST['groupe'] - 20
		$gr = $_POST['groupe'] - 20;
		$etu = $bdd->query("SELECT * FROM presence, etudiant WHERE NumEtu = NumEtudiant AND Annee = '$annee' and TP = '$gr'");
		$groupe = 'TP'.$gr;
	}



				$stats = 0;	// Un compteur de cours;
				$P = 0; // Variable présent
				$A = 0; // Variable absent

				foreach ($etu as $infos){
					if($infos['Absent'] == 0){
						$P = $P + 1;	// 0 = présent
					}
					if($infos['Absent'] == 1){
						$A = $A + 1;	//1 = absent
					}
				}
				
				$S = $P + $A;	//Variable somme cours abs + prés = cours créés
				
				if($S == 0){
					$m = 0; // Pourcentage = 0 pour éviter la division par 0
				}
				else{
				$m = ($A / $S)*100; //Pourcentage absence
				}
?>

<!DOCTYPE html>
	<head>
		<link href="css/presence.css" rel="stylesheet">
		<title> Enseignant </title>
		<meta charset = "utf-8">
	</head>

	<header>
		Cahier d'appel électronique
		<div class="logout">
			<form method='post' action="logout.php">
				<input type="submit" value="Se déconnecter" name="logout"></input>
			</form>
		</div>
	</header>

	<hr>
	

	<body>
		<h1> Liste des Étudiants </h1>
		<?php 
			
			//En-tête de la feuille de présence
			echo ('Module: '.$module.'<br/> Enseignement: '.$groupe.'<br>');

			//Tableau étudiants
			echo '<table border="1" width="200"><tr><td><b>Nom</b></td>'.'<td><b>Prenom</b></td></tr>';
			
			foreach ($etu as $donnees){
				echo "<tr><td>".$donnees['Nom']."</td><td> ".$donnees['Prenom']."</td></tr>";
			}
			
			echo '</table>';
		?>