<?php
	
	// Connexion à la BDD 
	try {
		$bdd = new PDO('mysql:host=localhost;dbname=pronote', 'root', '');
		
	}
	catch (exception $e){
		echo ("Erreur de connexion! Base de données inaccessible <br/>".$e) ;
	}

	// Variables type utilisateur
	$admin = $bdd->query("SELECT * FROM admin");
	$prof = $bdd->query("SELECT * FROM enseignant");
	$etudiant = $bdd->query("SELECT * FROM etudiant");

	//Variables de session
	$_SESSION['admin'] = 0;
	$_SESSION['prof'] = 0;
	$_SESSION['etudiant'] = 0;

	/* Vérification parmi les 3 profils utilisateurs */
	foreach($admin as $logA){
		if ($_POST['identifiant'] == $logA['MailAdmin']	&& $_POST['mdp'] == $logA['PassAdmin']){
				session_start();
				$_SESSION['admin'] = 1;
				header('Location: admin.php'); 		// Redirection page administration
				session_write_close();
		}

	}
	
	foreach($prof as $logP){
		if ($_POST['identifiant'] == $logP['MailProf']	&& $_POST['mdp'] == $logP['PassProf']){
				session_start();
				$_SESSION['prof'] = 1;
				header('Location: enseignant.php');	// Redirection page enseignant
				session_write_close();
		}
	}

	foreach($etudiant as $logE){
		if ($_POST['identifiant'] == $logE['MailEtudiant']	&& $_POST['mdp'] == $logE['PassEtudiant']){
				session_start();
				$_SESSION['etudiant'] = 1;
				$_SESSION['identifiant'] = $_POST['identifiant']; // Pour afficher des infos relatives à l'étudiant dans etudiant.php
				header('Location: etudiant.php');		// Redirection page élève
				session_write_close();
		}
	}

	// Si erreur authentification
	if($_SESSION['admin'] ==0 && $_SESSION['prof'] ==0 && $_SESSION['etudiant'] ==0){
		sleep(1);
		header('Location: login.php?login='.$_POST['identifiant']);
	}

	
?>
<html>
<link href="style.css" rel="stylesheet" type="text/css">
</html>