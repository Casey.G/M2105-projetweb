<?php

	session_start();
	if(($_SESSION['etudiant'] != 1)){
		sleep(1);
		header('Location: login.php?login='.$_POST['identifiant']); //Redirection si non authentifié
	}

	try {
			$bdd = new PDO('mysql:host=localhost;dbname=pronote', 'root', '');
	}
	
	catch (exception $e){
			echo ("Erreur de connexion! Base de données inaccessible <br/>".$e) ;
	}
?>

<!DOCTYPE html>
	<head>
		<title>	Etudiant </title>
		<link href="css/etudiant.css" rel="stylesheet">
		<meta charset = "utf-8">
	</head>

	<header>
		Cahier d'appel électronique
		<div class="logout">
			<form method='post' action="logout.php">
				<input type="submit" value="Se déconnecter" name="logout"></input>
			</form>
		</div>
	</header>

	<hr>

	<body>

		<?php

		date_default_timezone_set('Indian/Reunion'); //Définir fuseau horaire
		echo ("Nous sommes le :".date('d-m-Y')."<br>"); // Obtenir la date
		echo ("Il est : ".date('H')."h ".date('i')); //Obtenir l'heure
		
		$id = $_SESSION['identifiant']; //Correspond au mail dans le login

		$etu = $bdd -> query("SELECT * From presence, etudiant WHERE NumEtu = NumEtudiant AND mailEtudiant = '$id' ");
		
		?>

			<h1> Page ETUDIANT </h1>

			<?php
				$stats = 0;	// Un compteur de cours;
				$P = 0; // Variable présent
				$A = 0; // Variable absent

				foreach ($etu as $infos){
					if($infos['Absent'] == 0){
						$P = $P + 1;	// 0 = présent
					}
					if($infos['Absent'] == 1){
						$A = $A + 1;	//1 = absent
					}
				}
				
				$S = $P + $A;	//Variable somme cours abs + prés = cours créés
				
				if($S == 0){
					$m = 0; // Pourcentage = 0 pour éviter la division par 0
				}
				else{
				$m = ($A / $S)*100; //Pourcentage absence
				}

				echo ("Cours présent : ".$P." / Cours absent : ".$A."<br>");
				echo ("Votre taux d'absence est de ".$m."%");

			?>

	</body>
<html>