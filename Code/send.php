<?php
	session_start();
	try {
		$bdd = new PDO('mysql:host=localhost;dbname=pronote', 'root', '');
		
	}
	catch (exception $e){
		echo ("Erreur de connexion! Base de données inaccessible <br/>".$e) ;
	}

	date_default_timezone_set('Indian/Reunion'); //Définir fuseau horaire
?>
<!DOCTYPE html>
	<head>
		<link href="" rel="stylesheet">
		<title> Enseignant </title>
		<meta charset = "utf-8">
	</head>
	<body>

		<h2> Traitement en cours... </h2>
			
		<?php
		
			$j = 1; // indice du tableau 
			$c = count($_SESSION['t']); // on compte le nombre de lignes dans le tableau
			
			while ($j <$c){
				if($_POST[$j] == 0){	// Si présent
					$box = 0;			//présent
				}
				else{
					$box = 1;			//absent
				}
			
			$insert = $bdd -> prepare('INSERT INTO presence( Id, Nom, Prenom, Absent, le, heure, NumEtu) VALUES (:id, :Nom, :Prenom, :Absent, :le, :heure, :numetu)');
			$insert -> execute(array(
				'id' => date('d-m-Y-h-i-s').$j,		//On concatène la date + heure + secondes + itération => faibles chances de duplication dans la table
				'Nom' => $_SESSION['t'][$j]['nom'], 
				'Prenom' =>$_SESSION['t'][$j]['prenom'], 
				'Absent' => $box,
				'le' => date('Y-m-d'),
				'heure' => date('H-i'),
				'numetu' => $_SESSION['t'][$j]['id']));
				$j = $j + 1;
			}

			sleep(3);
			session_unset();
			session_destroy();
			header('Location:login.php');
		?>

	</body>
</html>