<?php
	if(!empty($_POST['logout'])){
		session_start();
		session_unset();  	// Réinitialise les variables session
		session_destroy();	// Détruit les variables session
		header('Location: login.php');
	}

?>